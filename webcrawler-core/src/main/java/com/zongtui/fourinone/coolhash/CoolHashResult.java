package com.zongtui.fourinone.coolhash;
public interface CoolHashResult{
	public CoolHashMap nextBatch(int batchLength);
}