<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/bar6">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/bar6" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                var option = {
                	    title : {
                	        text: 'ECharts2 vs ECharts1',
                	        subtext: 'Chrome下测试数据'
                	    },
                	    tooltip : {
                	        trigger: 'axis'
                	    },
                	    legend: {
                	        data:[
                	            'ECharts1 - 2k数据','ECharts1 - 2w数据','ECharts1 - 20w数据','',
                	            'ECharts2 - 2k数据','ECharts2 - 2w数据','ECharts2 - 20w数据'
                	        ]
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            magicType : {show: true, type: ['line', 'bar']},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    calculable : true,
                	    grid: {y: 70, y2:30, x2:20},
                	    xAxis : [
                	        {
                	            type : 'category',
                	            data : ['Line','Bar','Scatter','K','Map']
                	        },
                	        {
                	            type : 'category',
                	            axisLine: {show:false},
                	            axisTick: {show:false},
                	            axisLabel: {show:false},
                	            splitArea: {show:false},
                	            splitLine: {show:false},
                	            data : ['Line','Bar','Scatter','K','Map']
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'value',
                	            axisLabel:{formatter:'{value} ms'}
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'ECharts2 - 2k数据',
                	            type:'bar',
                	            itemStyle: {normal: {color:'rgba(193,35,43,1)', label:{show:true}}},
                	            data:[40,155,95,75, 0]
                	        },
                	        {
                	            name:'ECharts2 - 2w数据',
                	            type:'bar',
                	            itemStyle: {normal: {color:'rgba(181,195,52,1)', label:{show:true,textStyle:{color:'#27727B'}}}},
                	            data:[100,200,105,100,156]
                	        },
                	        {
                	            name:'ECharts2 - 20w数据',
                	            type:'bar',
                	            itemStyle: {normal: {color:'rgba(252,206,16,1)', label:{show:true,textStyle:{color:'#E87C25'}}}},
                	            data:[906,911,908,778,0]
                	        },
                	        {
                	            name:'ECharts1 - 2k数据',
                	            type:'bar',
                	            xAxisIndex:1,
                	            itemStyle: {normal: {color:'rgba(193,35,43,0.5)', label:{show:true,formatter:function(p){return p.value > 0 ? (p.value +'\n'):'';}}}},
                	            data:[96,224,164,124,0]
                	        },
                	        {
                	            name:'ECharts1 - 2w数据',
                	            type:'bar',
                	            xAxisIndex:1,
                	            itemStyle: {normal: {color:'rgba(181,195,52,0.5)', label:{show:true}}},
                	            data:[491,2035,389,955,347]
                	        },
                	        {
                	            name:'ECharts1 - 20w数据',
                	            type:'bar',
                	            xAxisIndex:1,
                	            itemStyle: {normal: {color:'rgba(252,206,16,0.5)', label:{show:true,formatter:function(p){return p.value > 0 ? (p.value +'+'):'';}}}},
                	            data:[3000,3000,2817,3000,0]
                	        }
                	    ]
                	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>