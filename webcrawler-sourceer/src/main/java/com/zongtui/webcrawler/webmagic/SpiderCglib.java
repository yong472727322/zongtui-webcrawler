/**
 * Project Name:webcrawler-sourceer
 * File Name:SpiderCglib.java
 * Package Name:com.zongtui.webcrawler.webmagic
 * Date:2015年4月26日下午10:32:23
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */
package com.zongtui.webcrawler.webmagic;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * ClassName: SpiderCglib <br/>
 * Function: webmgaic spider代理类. <br/>
 * date: 2015年4月26日 下午10:32:23 <br/>
 *
 * @author sky
 * @version
 * @since JDK 1.7
 */
public class SpiderCglib implements MethodInterceptor {
	private Object target;

	/**
	 * 创建代理对象
	 * 
	 * @param target
	 * @return
	 */
	public Object getInstance(Object target) {
		this.target = target;
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(this.target.getClass());
		// 回调方法
		enhancer.setCallback(this);
		// 创建代理对象
		return enhancer.create();
	}

	@Override
	// 回调方法
	public Object intercept(Object obj, Method method, Object[] args,
			MethodProxy proxy) throws Throwable {
		System.out.println("事物开始");
		proxy.invokeSuper(obj, args);
		System.out.println("事物结束");
		return null;

	}

}
